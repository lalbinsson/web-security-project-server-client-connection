### Question A: What does the OpenSSL switch -CAcreateserial do?
The flag creates a serial number file if it does not exist.
A serial number uniquely identifies a certificate signed by a CA Authority.
These need to be unique and are saved in a file so that they are not repeated.

### Question B: How can you tell keytool to generate a CSR for an X.509 version 3 client certificate, or tell OpenSSL to force generation of a version 3 certificate?

Openssl need an exten file containing a couple more settings:
-extfile v3.ext 

where v3.ext:
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment

### Question C: What are extensions and what can they contain?
Extensions are there to restrict the use of the certificate.
Examples of this would be to restrict the public key to be used for signatures but not for encryption.
We could also restrict its use to an TSL connection, or that the key is used exclusively for email

### Question D: Is it possible to just make a copy of the client-side truststore, why or why not?
Yes, it was possible to copy the truststore with cp and then use the keytool commands as normal.
The certificates in the truststore contains information about which machine created them,
but as we copied our truststore on the same machine, the information is still valid.
Copying the truststore to a different machine could still work but some clients might complain that the certificates are signed by a different machine.

### Question E: What is the purpose of each password? That is, what does each password protect?
The truststore passwords protect the confidentiality and integrity of the trusted certificates

The keystore passwords protect the confidentiality and integrity of the keystore
and the other passwords protect the key in the keystore


### Question F:
The server prints back the reversed string.


### Question G:
It forces the SSL socket to require authentication.


### Question H:
Server Started

client connected
client name (cert subject DN field): CN=Hampus Weslien (ha8040we-s)/Tobias Bladh (ast15tbl)/Lucy Albinsson (lu2767al-s)/Tove Solve (to5185so-s), OU=., O=LTH, L=Lund, ST=Skåne, C=SE
Issuer:
EMAILADDRESS=ha8040we-s@gmail.com, CN=ca, O=LTH, L=Lund, ST=SkÃ¥ne, C=SE

Serial:
16177708292934980768

1 concurrent connection(s)

-----

args[0] = localhost
args[1] = 9876

socket before handshake:
16c0663d[SSL_NULL_WITH_NULL_NULL: Socket[addr=localhost/127.0.0.1,port=9876,localport=39708]]

certificate name (subject DN field) on certificate received from server:
CN=MyServer, OU=., O=LTH, L=Lund, ST=Skåne, C=SE

Issuer:
EMAILADDRESS=ha8040we-s@gmail.com, CN=ca, O=LTH, L=Lund, ST=SkÃ¥ne, C=SE

Serial:
16177708292934980769

socket after handshake:
16c0663d[TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384: Socket[addr=localhost/127.0.0.1,port=9876,localport=39708]]

secure connection established

