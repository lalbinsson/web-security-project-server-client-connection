#!/bin/bash

#sudo apt-get install openssl

#generate key and certificate
openssl req -newkey rsa:2048 -nodes -keyout ca.key -x509 -days 365 -out ca.crt
# add certificate to the clienttruststore
keytool -import -file ca.crt  -alias ca -keystore clienttruststore
# generate a a key pair and add it to the clientkeystore
keytool -genkeypair -alias client -keyalg RSA -keystore clientkeystore -keysize 2048
#Hampus Weslien (ha8040we-s)/Tobias Bladh (ast15tbl)/Lucy Albinsson (lu2767al-s)/Tove Solve (to5185so-s)
#generate a certificate signing request with the client key
keytool -certreq -alias client -keystore clientkeystore -file client.csr
#sign the certificate
openssl x509 -in client.csr -out client.crt -req -CA ca.crt -CAkey ca.key -days 365 -CAcreateserial
#import the certificates
keytool -import -trustcacerts -alias ca -file ca.crt -keystore clientkeystore
keytool -import -trustcacerts -alias client -file client.crt -keystore clientkeystore

#Generate server-side keystore
keytool -genkeypair -alias server -keyalg RSA -keystore serverkeystore -keysize 2048 
#-dname MyServer
#create a certificate to be signed
keytool -certreq -alias server -keystore serverkeystore -file server.csr
#sign the server's certificate
openssl x509 -in server.csr -out server.crt -req -CA ca.crt -CAkey ca.key -days 365 -CAcreateserial
#import the certificates
keytool -import -trustcacerts -alias ca -file ca.crt -keystore serverkeystore
keytool -import -trustcacerts -alias server -file server.crt -keystore serverkeystore
#generate server truststore
keytool -import -file ca.crt  -alias ca -keystore servertruststore
