package server;

import common.JournalRecord;
import common.messages.Reply;
import common.messages.Request;
import server.Security.Validator;
import server.User.User;
import server.database.JournalDatabase;

import javax.net.ssl.SSLSocket;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.Security;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class UserHandler implements Runnable {

    User user;
    SSLSocket sslSocket;
    ObjectInputStream in;
    ObjectOutputStream out;
    JournalDatabase jdb;

    public UserHandler(User user, SSLSocket sslSocket, JournalDatabase jdb) throws IOException {
        this.user = user;
        this.sslSocket = sslSocket;
        out = new ObjectOutputStream(sslSocket.getOutputStream());
        in = new ObjectInputStream(sslSocket.getInputStream());
        this.jdb = jdb;
    }

    @Override
    public void run() {
        Logger.logUserConnect(user);
        try {
            while (true) {
                Request request = (Request) in.readObject();
                List<JournalRecord> records = getRecord(request);
                Reply reply = getReply(request, user, records);
                if (reply.isValid()) {
                    String replyMsg = executeRequest(request, records, user);
                    reply.addMsg(replyMsg);
                    reply.setRecord(records.get(0));
                }
                out.writeObject(reply);
            }
        } catch (EOFException e) {
            System.out.println("Socket has been closed by client.");
        } catch (Exception e){
            e.printStackTrace();
            System.out.println("Connection has been lost unexpectedly.");
        } finally {
            jdb.saveToFile();
            Logger.logUserDisconnect(user);
            System.out.println("Closing thread.");
        }
    }

    private String executeRequest(Request req, List<JournalRecord> records, User user) {
        String replyMsg = "";

        if (req.isRead()) {
            replyMsg = records.get(0).toString();
        } else if (req.isDelete()) {
            replyMsg = executeDelete(req);
        } else if (req.isCreate()) {
            replyMsg = executeCreate(req, user);
        } else if (req.isWrite()) {
            replyMsg = executeWrite(req);
        } else if (req.isList()) {
            replyMsg = executeList(records);
        }

        return replyMsg;
    }

    private String executeList(List<JournalRecord> records){
        String ls = "";
        for (JournalRecord record: records) {
            ls = ls + record.getReferenceString() + "\n";
        }
        return ls;
    }

    private String executeDelete(Request req){
        int ref = req.getJournalReference();
        jdb.deleteEntry(ref);
        jdb.saveToFile();
        return "Successfully deleted journal: " + ref;
    }

    private String executeWrite(Request req){
        int ref = req.getJournalReference();
        jdb.updateEntry(ref, req.getJournal());
        jdb.saveToFile();
        return "Successfully updated journal: " + ref;
    }

    private String executeCreate(Request req, User user){
        int ref = jdb.getAvailableJournalID();
        JournalRecord journalRecord = req.getJournal();
        journalRecord.addReferenceAndUserData(ref, user);

        jdb.updateEntry(ref, journalRecord);
        jdb.saveToFile();
        return "Successfully created journal: " + ref;
    }

    private Reply getReply(Request request, User user, List<JournalRecord> records){
        if(request.isList()){
            Validator.filterValidRecords(request, user, records);
            return new Reply(!records.isEmpty(), "You are not authorized to view any records");
        } else {
            //if it is not a list request we will only have one record
            return Validator.checkValidOperation(request, user, records.get(0));
        }
    }

    private List<JournalRecord> getRecord(Request req){
        JournalRecord record;
        if (req.isWrite() || req.isCreate()) record = req.getJournal();
        else if (req.isList()) return new LinkedList<>(jdb.getAll());
        else record = jdb.getEntry(req.getJournalReference());

        LinkedList<JournalRecord> records = new LinkedList<>();
        records.add(record);
        return records;
    }

}
