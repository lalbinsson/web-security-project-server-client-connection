package server;

import common.SSLContextBuilder;
import server.User.User;
import server.database.JournalDatabase;
import server.database.UserDatabase;

import javax.net.ssl.*;
import java.io.IOException;
import javax.security.cert.X509Certificate;

public class Server implements Runnable {
    private SSLServerSocket ss = null;

    private String password = "password";
    private String username = "server";

    UserDatabase userDatabase = new UserDatabase();
    JournalDatabase journalDatabase = new JournalDatabase();

    public Server(int port) throws IOException {
        SSLContext ctx = SSLContextBuilder.fromUser(username, password);
        SSLServerSocketFactory ssf = ctx.getServerSocketFactory();
        ss = (SSLServerSocket) ssf.createServerSocket(port);
        ss.setNeedClientAuth(true); // enables client authentication
        journalDatabase.init();
    }

    @Override
    public void run() {
        try {
        while (true) {
            SSLSocket socket = (SSLSocket) ss.accept();
            SSLSession session = socket.getSession();
            X509Certificate cert = session.getPeerCertificateChain()[0];

            userDatabase.init();
            User user = userDatabase.getEntry(cert.getSerialNumber().toString());
            userDatabase.saveToFile();

            UserHandler handler = new UserHandler(user, socket, journalDatabase);
            new Thread(handler).start();

        }
        } catch (IOException e) {
            System.out.println("Client died: " + e.getMessage());
            e.printStackTrace();
        } finally {
            //realse socket ffs
        }
    }

}
