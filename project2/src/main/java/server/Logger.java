package server;

import common.messages.Request;
import server.User.User;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

public class Logger {
    private static File file;


    public enum Outcome {ACCEPTED, DENIED}

    public static void initLogger() {
        file = new File("resources/log/log.txt");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.out.println("Error creating logRequestAndOutcome file");
                e.printStackTrace();
            }
        }
    }

    public static void logRequestAndOutcome(Outcome outcome, Request request, User user) {
        logUserAction(user, request + " : " + outcome);

    }

    public static void logUserConnect(User user){
        logUserAction(user, "connect");
    }

    public static void logUserDisconnect(User user){
        logUserAction(user, "disconnect");
    }

    private static void logUserAction(User user, String info){
        FileWriter fileWriter;
        StringBuilder bob = new StringBuilder();
        bob.append(new Date().toString());
        bob.append(" - ");
        bob.append("User ");
        bob.append(user.getUserID());
        bob.append(" " + info +"\n");
        System.out.println(bob.toString());

        try {
            fileWriter = new FileWriter(file, true);
            fileWriter.write(bob.toString());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Error appending to logfile!");
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Logger logger = new Logger();

        //logger.logRequestAndOutcome(Outcome.ACCEPTED, Request.createReadMsg(12345678), User.createUser("duh", "division111", 111, 123));
        //logger.logRequestAndOutcome(Outcome.DENIED, Request.createReadMsg(12345678), User.createUser("duh", "division111", 111, 123));

    }
}
