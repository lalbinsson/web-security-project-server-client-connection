package server.User;

import java.math.BigInteger;

public class User {
    public static final int DOCTOR = 0;
    public static final int NURSE = 1;
    public static final int GOVERNMENT = 2;
    public static final int PATIENT = 3;

    private int userID;
    private int divisionID;
    private String certificateSerial;

    private String username;
    private String division;
    private int userType;

    private User(String username, String division, int userType, int userID, int divisionID, String certificateSerial) {
        this.username = username;
        this.division = division;
        this.userType = userType;
        this.userID = userID;
        this.divisionID = divisionID;
        this.certificateSerial = certificateSerial;
    }

    public String getUsername() {
        return username;
    }

    public String getDivision() {
        return division;
    }

    public int getUserType() {
        return userType;
    }

    public static User createNurse(String username, String division, int userID, int divisionID, String certificateSerial) {
        return new User(username, division, NURSE, userID, divisionID, certificateSerial);
    }

    public static User createDoctor(String username, String division, int userID, int divisionID, String certificateSerial) {
        return new User(username, division, DOCTOR, userID, divisionID, certificateSerial);
    }

    public static User createGovernment(String username, String division, int userID, int divisionID, String certificateSerial) {
        return new User(username, division, GOVERNMENT, userID, divisionID, certificateSerial);
    }
    public static User createPatient(String username, String division, int userID, int divisionID, String certificateSerial) {
        return new User(username, division, PATIENT, userID, divisionID, certificateSerial);
    }

    public int getUserID() {
        return userID;
    }

    public int getDivisionID() {
        return divisionID;
    }

    public String getCertificateSerial() {return certificateSerial; }
}
