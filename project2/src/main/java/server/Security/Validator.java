package server.Security;

import common.JournalRecord;
import common.messages.Reply;
import common.messages.Request;
import server.Logger;
import server.User.User;

import java.util.List;

public class Validator {

    public static void filterValidRecords(Request msg, User user, List<JournalRecord> records) {
        records.removeIf(record -> !rules(msg, user, record).isValid());
        Logger.logRequestAndOutcome(!records.isEmpty() ? Logger.Outcome.ACCEPTED : Logger.Outcome.DENIED, msg, user);
    }


    public static Reply checkValidOperation(Request msg, User user, JournalRecord jr) {
        Reply reply = jr != null ? rules(msg, user, jr) : new Reply(false, "Invalid journal reference.");
        Logger.logRequestAndOutcome(reply.isValid() ? Logger.Outcome.ACCEPTED : Logger.Outcome.DENIED, msg, user);
        return reply;
    }

    private static Reply rules(Request msg, User usr, JournalRecord jr) {
        Reply reply = new Reply(false, "Invalid User type");
        switch(usr.getUserType()) {
            case User.NURSE:
                reply = nurseRules(msg, usr, jr);
                break;
            case User.DOCTOR:
                reply = doctorRules(msg, usr, jr);
                break;
            case User.GOVERNMENT:
                reply = governmentRules(msg);
                break;
            case User.PATIENT:
                reply = patientRules(msg, usr, jr);
                break;
        }
        return reply;

    }



/*
    nurse can read and write to all record they are associated with
    either by being the associated nurse
    or the associated division
 */
    private static Reply nurseRules(Request msg, User usr, JournalRecord jr){
        String failMsg = "";

        boolean validReq = msg.isRead() || msg.isWrite() || msg.isList();
        boolean validJr = usr.getUserID() == jr.getNurseID() || usr.getDivisionID() == jr.getDivisionID();


        boolean validReadOrList = (msg.isRead() || msg.isList()) && validJr;
        boolean validWrite      = (msg.isWrite() && usr.getUserID() == jr.getNurseID());

        boolean valid = validReadOrList || validWrite;

        if (!validReq) failMsg = denyMsg("Request wasn't a READ, WRITE or LIST request.");
        else if (!valid) failMsg = denyMsg("You are not allowed to perform this action on this record.");

        return new Reply(valid, failMsg);

    }
    private static Reply doctorRules(Request msg, User usr, JournalRecord jr){
        String failMsg = "";
        boolean readWrite = (msg.isRead() || msg.isWrite() || msg.isList());
        boolean create = msg.isCreate();
        boolean isAssociated = usr.getUserID() == jr.getDoctorID();
        boolean readDivision = (msg.isRead() || msg.isList()) && usr.getDivisionID() == jr.getDivisionID();

        if (!(readWrite || create)) failMsg = denyMsg("Request wasn't a READ, WRITE, LIST or CREATE request.");
        else if(!(isAssociated || readDivision) )failMsg = denyMsg("You are not associated with this record.");

        boolean valid = (readWrite && isAssociated) || readDivision || create;

        return new Reply(valid, failMsg);
    }
    private static Reply governmentRules(Request msg){
        boolean valid = msg.isRead() || msg.isDelete() || msg.isList();
        return new Reply(valid, denyMsg("Request wasn't a READ, DELETE or LIST request."));
    }
    private static Reply patientRules(Request msg, User usr, JournalRecord jr){
        String failMsg = "";
        boolean validReq = msg.isRead() || msg.isList();
        boolean validJr = usr.getUserID() == jr.getPatientID();

        if (!validReq) failMsg = denyMsg("Request wasn't a READ or List request.");
        else if(!validJr) failMsg = denyMsg("You are not associated with this record.");

        boolean valid =  validReq && validJr;

        return new Reply(valid, failMsg);
    }

    private static String denyMsg(String msg){
        return "DENIED: " + msg + "\n";
    }
}
