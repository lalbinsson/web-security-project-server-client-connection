package server.database;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import common.JournalRecord;

import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Type;
import java.util.*;

abstract class Database<Key, Entry> {
    protected Gson gson;
    protected File dataFile;
    protected HashMap<Key, Entry> data;

    public Database(String fileName) {
        gson = new Gson();
        dataFile = new File(fileName);
    }

    /**
     * Supposed to be run once before database operations are being made
     * in order to store all the data in memory for easy access.
     */
    public void init() {
        String entireFileText = "";
        try {
            if (!dataFile.exists()) {
                dataFile.createNewFile();
            }
            entireFileText = new Scanner(dataFile)
                    .useDelimiter("\\A").next();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        Type type = new TypeToken<LinkedHashMap<Key, Entry>>(){}.getType();
        data = gson.fromJson(entireFileText, type);
    }

    /**
     * Run before closing program in order to save changes to harddrive.
     */
    public void saveToFile() {
        String entireFileText = gson.toJson(data);
        try {
            FileWriter fileWriter = new FileWriter(dataFile,false);
            fileWriter.write(entireFileText);
            fileWriter.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Collection<Entry> getAll() { return data.values(); }

    public Entry getEntry(Key reference) {
        return data.get(reference);
    }

    public Entry updateEntry(Key reference, Entry record) {
        return data.put(reference, record);
    }

    public Entry deleteEntry(Key reference) { return data.remove(reference); }
}
