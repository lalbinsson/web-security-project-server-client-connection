package server.database;


import com.google.gson.reflect.TypeToken;
import server.User.User;

import javax.security.cert.X509Certificate;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.util.LinkedHashMap;
import java.util.Scanner;

/**
 * This database uses a certificate serial number string to access the users.
 * The users still have a personal integer ID as well.
 */
public class UserDatabase extends Database<String, User> {

    public UserDatabase() {
        super("resources/databases/users");
    }


    /**
     * Use to populate database!
     */
    public static void main(String[] args) {

        UserDatabase db = new UserDatabase();
        db.init();

        String serial = getSerial("hampus");
        User user = User.createDoctor("hampus", "lund", 1, 100, serial);
        db.updateEntry(serial.toString(), user);

        serial = getSerial("lucy");
        user = User.createDoctor("lucy", "göteborg", 2, 200, serial);
        db.updateEntry(serial.toString(), user);

        serial = getSerial("NSA");
        user = User.createGovernment("NSA", "usa", 999, 999, serial);
        db.updateEntry(serial.toString(), user);

        serial = getSerial("tobbe");
        String testSerialTobbe = serial;

        user = User.createNurse("tobbe", "göteborg", 3, 200, serial);
        db.updateEntry(serial.toString(), user);

        serial = getSerial("tove");
        user = User.createNurse("tove", "lund", 4, 100, serial);
        db.updateEntry(serial.toString(), user);

        serial = getSerial("lars");
        user = User.createPatient("lars", "lund", 5, 100, serial);
        db.updateEntry(serial.toString(), user);

        serial = getSerial("amanda");
        user = User.createPatient("amanda", "göteborg", 6, 200, serial);
        db.updateEntry(serial.toString(), user);

        serial = getSerial("rickard");
        user = User.createPatient("rickard", "lund", 7, 100, serial);
        db.updateEntry(serial.toString(), user);

        serial = getSerial("nina");
        user = User.createPatient("nina", "göteborg", 8, 200, serial);
        db.updateEntry(serial.toString(), user);

        db.saveToFile();

        db = new UserDatabase();
        db.init();

        User probsTobbe = db.getEntry(testSerialTobbe);
        System.out.println("should be tobbe: " + probsTobbe.getUsername());

        User probsTove = db.getEntry(serial);
        System.out.println("should be tove: " + probsTove.getUsername());

        db.saveToFile();
    }

    @Override
    public void init() {
        String entireFileText = "";
        try {
            if (!dataFile.exists()) {
                dataFile.createNewFile();
            }
            entireFileText = new Scanner(dataFile)
                    .useDelimiter("\\A").next();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        Type type = new TypeToken<LinkedHashMap<String, User>>(){}.getType();
        data = gson.fromJson(entireFileText, type);
    }

    /**
     * Gets the serial number from certificate of user
     * @param name username of the user
     * @return A BigInteger or null if things are not nice.
     */
    private static String getSerial(String name) {
        try {
            InputStream inStream = new FileInputStream("resources/security/certificates/" + name +".certificate.pem");
            X509Certificate cert = X509Certificate.getInstance(inStream);

            BigInteger serial = cert.getSerialNumber();

            System.out.println(serial);

            inStream.close();

            return serial.toString();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
