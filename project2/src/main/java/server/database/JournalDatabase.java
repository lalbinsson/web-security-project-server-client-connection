package server.database;

import com.google.gson.reflect.TypeToken;
import common.JournalRecord;

import java.lang.reflect.Type;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class JournalDatabase extends Database<Integer, JournalRecord> {

    public JournalDatabase() {
        super("resources/databases/journals");
    }

    /**
     * Ugly search functions coming below!
     * Could be used to let client search for records,
     * but note that this has no security and each record
     * has to be checked before sending.
     **/
    public List<JournalRecord> searchByDoctorID(int id) {
        return data.values().stream().filter(record -> id == record.getDoctorID()).collect(Collectors.toList());
    }

    public List<JournalRecord> searchByNurseID(int id) {
        return data.values().stream().filter(record -> id == record.getNurseID()).collect(Collectors.toList());
    }

    public List<JournalRecord> searchByPatientID(int id) {
        return data.values().stream().filter(record -> id == record.getPatientID()).collect(Collectors.toList());
    }

    public List<JournalRecord> searchByDivisionID(int id) {
        return data.values().stream().filter(record -> id == record.getDivisionID()).collect(Collectors.toList());
    }


    @Override
    public void init() {
        String entireFileText = "";
        try {
            if (!dataFile.exists()) {
                dataFile.createNewFile();
            }
            entireFileText = new Scanner(dataFile)
                    .useDelimiter("\\A").next();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        Type type = new TypeToken<Map<Integer, JournalRecord>>(){}.getType();
        data = gson.fromJson(entireFileText, type);
    }

    public static void main(String[] args) {
        //little test!
        JournalDatabase database = new JournalDatabase();
        database.init();

        database.updateEntry(1, new JournalRecord(1, 1, 1,1,1,"shits really effing bad yo", new Date(), new Date()));

        database.updateEntry(2, new JournalRecord(2, 1, 1,1,1,"shits good f yo", new Date(), new Date()));

        database.updateEntry(9, new JournalRecord(9, 1, 1,1,1,"shits w f yo", new Date(), new Date()));

        JournalRecord jr = database.getEntry(9);

        System.out.println(jr.toString());

        database.saveToFile();
    }

    /**
     * Cheap solution, I know.
     * @return random integer id-number
     */
    public int getAvailableJournalID() {
        while(true) {
            int randomID = ThreadLocalRandom.current().nextInt(1000, 10000);
            if (!data.containsKey(randomID)) {
                return randomID;
            }
        }
    }
}