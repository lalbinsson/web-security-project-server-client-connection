package common.messages;

import common.JournalRecord;

import java.io.Serializable;

public class Reply implements Serializable {

    boolean valid;
    JournalRecord record;
    private String msg;

    public Reply(boolean valid, String failMsg) {
        this.valid = valid;
        this.msg = failMsg;
    }

    public String renderMsg(){
        return msg;
    }

    public void validdate(boolean valid, String failMsg){
        if (!valid)  msg = failMsg;
    }

    public boolean isValid() {
        return valid;
    }

    public void addMsg(String msg) {
        this.msg = msg;
    }

    public JournalRecord getJournalRecord() {
        return record;
    }

    public void setRecord(JournalRecord record) {
        this.record = record;
    }
}
