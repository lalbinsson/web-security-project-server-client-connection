package common.messages;

import common.JournalRecord;

import java.io.Serializable;

public class Request implements Serializable {
    public static final int READ = 0;
    public static final int WRITE = 1;
    public static final int CREATE = 2;
    public static final int DELETE = 3;
    public static final int LIST = 4;

    private int requestType;
    private int journalReference;
    private JournalRecord journal;

    private Request(int journalReference, JournalRecord journal, int requestType) {
        this.journalReference = journalReference;
        this.journal = journal;
        this.requestType = requestType;
    }

    public static Request createReadMsg(int journalReference) {
        return new Request(journalReference, null, READ);
    }
    public static Request createWriteMsg(int journalReference, JournalRecord journal) {
        return new Request(journalReference, journal, WRITE);
    }
    public static Request createCreateMsg(int journalReference, JournalRecord journal) {
        return new Request(journalReference, journal, CREATE);
    }
    public static Request createDeleteMsg(int journalReference) {
        return new Request(journalReference, null, DELETE);
    }

    public static Request createListMsg() {
        return new Request(-1, null, LIST);
    }

    public int getJournalReference() {
        return journalReference;
    }

    public JournalRecord getJournal() {
        return journal;
    }

    public boolean isRead(){return requestType == READ;}
    public boolean isWrite(){return requestType == WRITE;}
    public boolean isCreate(){return requestType == CREATE;}
    public boolean isDelete(){return requestType == DELETE;}
    public boolean isList(){return requestType == LIST;}

    @Override
    public String toString() {
        String types[] = new String[]{"READ", "WRITE", "CREATE", "DELETE", "LIST"};
        return "request " +
                (requestType >= 0 && requestType < types.length ? types[requestType] : "UNKNOWN") +
                " of document " + journalReference;
    }
}
