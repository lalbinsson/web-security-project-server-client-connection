package common;

import server.User.User;

import java.io.Serializable;
import java.util.Date;

public class JournalRecord implements Serializable {
    private int referenceNumber;
    private int doctorID;
    private int nurseID;
    private int divisionID;
    private int patientID;
    private String medicalData;

    private Date creationDate;
    private Date lastEditedDate;

    public JournalRecord(int referenceNumber, int doctorID, int nurseID, int divisionID, int patientID,
                         String medicalData, Date creationDate, Date lastEditedDate) {
        this.referenceNumber = referenceNumber;
        this.doctorID = doctorID;
        this.nurseID = nurseID;
        this.divisionID = divisionID;
        this.medicalData = medicalData;
        this.creationDate = creationDate;
        this.lastEditedDate = lastEditedDate;
        this.patientID = patientID;
    }

    public JournalRecord(int referenceNumber, int doctorID, int nurseID, int divisionID, int patientID,
                         String medicalData) {
        this(referenceNumber,doctorID,nurseID,divisionID,patientID,medicalData,new Date(), new Date());
    }

    public void appendMedicalData(String append) {
        medicalData = medicalData + "\n----------------------\n" + append;
    }

    public void updateLastEdit() {
        lastEditedDate = new Date();
    }

    public int getDoctorID() {
        return doctorID;
    }

    public int getNurseID() {
        return nurseID;
    }

    public int getDivisionID() {
        return divisionID;
    }

    public int getPatientID() {
        return patientID;
    }

    public String getMedicalData() {
        return medicalData;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public Date getLastEditedDate() {
        return lastEditedDate;
    }

    public String getReferenceString(){
        return Integer.toString(referenceNumber);
    }

    public int getReferenceNumber() {
        return referenceNumber;
    }

    public boolean addReferenceAndUserData(int referenceNumber, User user) {
        if (this.referenceNumber == -1 && this.doctorID == -1) {
            this.referenceNumber = referenceNumber;
            this.doctorID = user.getUserID();
            this.divisionID = user.getDivisionID();
            return true;
        } else return false;
    }

    @Override
    public String toString() {
        return "\nJournal record - ref: " + referenceNumber +
                "\nCreated:  " + creationDate + ", and last edited: " + lastEditedDate +
                "\nDivision: " + divisionID +
                "\nDoctor:   " + doctorID +
                "\nNurse:    " + nurseID +
                "\nPatient:  " + patientID +
                "\n----------------------\n" +
                medicalData +
                "\n----------------------\n";
    }
}
