package common;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.PrivateKey;

/**
 * The plan is to let this class handle the two factor authentication.
 */

public class SSLContextBuilder {

    /**
     * This method is intended to return the private key of a keystore based on a keyStoreName and
     * a password, both associated with a specific user.
     * @param keyStoreName
     * @param password
     * @return
     */
    private static String path = "resources/security/keystores/";

    public static javax.net.ssl.SSLContext fromUser(String username, String password) {
        String ksname = path + username + "KeyStore.jks";
        String tsname = path + username + "TrustStore.jks";

        return createSSLContext(ksname,tsname,password.toCharArray());
    }

    public static javax.net.ssl.SSLContext createSSLContext(String keyStoreName, String trustStoreName, char[] password) {
        try {
            KeyStore ks = KeyStore.getInstance("JKS");
            KeyStore ts = KeyStore.getInstance("JKS");
            KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
            TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
            javax.net.ssl.SSLContext ctx = javax.net.ssl.SSLContext.getInstance("TLS");
            ks.load(new FileInputStream(keyStoreName), password);  // keystore password (storepass)
            ts.load(new FileInputStream(trustStoreName), password); // truststore password (storepass);
            kmf.init(ks, password); // user password (keypass)
            tmf.init(ts); // keystore can be used as truststore here
            ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

            return ctx;

        } catch (java.security.KeyStoreException e) {
            System.out.println("Something went wrong while instantiating keystore.");
            System.exit(1);
        } catch(FileNotFoundException e) {
            System.out.println("There is no keystore/valuestore associated with that username");
            System.exit(1);
        } catch(IOException e){
            System.out.println("Incorrect password");
            System.exit(1);
        } catch (Exception e) {
            System.exit(1);
        }
        return null; //går ändå inte att komma hit men java har damp
    }
}
