package client;

import common.JournalRecord;
import common.messages.Reply;
import common.messages.Request;

import javax.net.ssl.SSLSocket;
import java.io.Console;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.SQLOutput;
import java.util.Scanner;

public class App {
    private static boolean DEBUG = false;

    public static void main(String[] args) {
        ClientConnector client = new ClientConnector("localhost", 5544);
        Scanner scanner = new Scanner(System.in);
        Console console = System.console();

        System.out.print("Enter your username in order to use your individual keystore. \n > ");
        String username = scanner.nextLine();
        String password;
        if (!DEBUG) password = new String(console.readPassword("Enter your password > ")); //Doesnt work in intellij
        else {
            System.out.print("\nOBS DEBUG OBS! Enter password > ");
            password = scanner.nextLine();
        }

        try {
            SSLSocket socket = client.createSocket(username, password);
            socket.startHandshake();

            ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(socket.getInputStream());

            System.out.println("Connection was successful. Use help to get help.");


            boolean running = true;
            while (running) {
                System.out.print("> ");
                String tokens[] = scanner.nextLine().split(" ");
                Request request;
                switch (tokens[0].toLowerCase()) {
                    case "q":
                    case "quit":
                        socket.close();
                        System.out.println("Connection will be terminated. Goodbye!");
                        running = false;
                        break;
                    case "read":
                        request = Request.createReadMsg(Integer.parseInt(tokens[1]));
                        out.writeObject(request);
                        awaitAndPrintReply(in);
                        break;
                    case "write":
                        sendWriteRequest(scanner, in, out, tokens);
                        break;
                    case "create":
                        sendCreateRequest(scanner, in, out);
                        break;
                    case "delete":
                        sendDeleteRequest(in, out, tokens);
                        break;
                    case "ls":
                        request = Request.createListMsg();
                        out.writeObject(request);
                        awaitAndPrintReply(in);
                        break;

                }
            }

            socket.close();
        } catch (IOException e) {
            System.out.println("Failed setting up connection to server. Please try again.");
        }
    }

    private static void sendCreateRequest(Scanner scanner, ObjectInputStream in, ObjectOutputStream out) {
        System.out.println("A new journal record will be created in your name.");
        int nurseID = scanIDHelper(scanner, "Enter nurse ID");
        int patientID = scanIDHelper(scanner, "Enter patient ID");
        String medicalData = scanHelper(scanner, "Enter initial medical data");

        JournalRecord jr = new JournalRecord(-1, -1, nurseID, -1, patientID,medicalData);
        Request request = Request.createCreateMsg(0, jr);

        try {
            out.writeObject(request);
        } catch (IOException e) {
            System.out.println("Something went wrong while sending create message");
        }

        awaitAndPrintReply(in);
    }

    private static String scanHelper(Scanner scanner, String message) {
        System.out.print(message + " > ");
        return scanner.nextLine();
    }

    private static int scanIDHelper(Scanner scanner, String message) {
        String s = scanHelper(scanner, message);
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException e) {
            System.out.println("Number format is invalid. Please try again.");
            return scanIDHelper(scanner, message);
        }
    }

    private static JournalRecord updateRecordLocally(Scanner scanner, JournalRecord prev) {
        System.out.print("Append medical data > ");
        String append = scanner.nextLine();

        prev.appendMedicalData(append);
        prev.updateLastEdit();

        return prev;
    }

    private static void sendDeleteRequest(ObjectInputStream in, ObjectOutputStream out, String[] tokens) {
        if (tokens.length < 2) {
            System.out.println("Syntax error.");
            return;
        }
        Request request = Request.createDeleteMsg(Integer.parseInt(tokens[1]));

        try {
            out.writeObject(request);
        } catch (IOException e) {
            System.out.println("Error while sending request.");
        }
        awaitAndPrintReply(in);
    }

    private static void sendWriteRequest(Scanner scanner, ObjectInputStream in, ObjectOutputStream out, String[] tokens) {
        if (tokens.length < 2) {
            System.out.println("Syntax error.");
            return;
        }
        Request request = Request.createReadMsg(Integer.parseInt(tokens[1]));
        try {
            out.writeObject(request);
        } catch (IOException e) {
            System.out.println("Error sending internal read request in write sequence. Server possibly down.");
            return;
        }
        try {
            Reply reply = (Reply) in.readObject();

            System.out.println(reply.renderMsg());
            if (!reply.isValid()) {
                return;
            }

            JournalRecord record = updateRecordLocally(scanner, reply.getJournalRecord());
            request = Request.createWriteMsg(record.getReferenceNumber(), record);
            out.writeObject(request);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error while retrieving read response from server, or sending write request.");
            return;
        }

        awaitAndPrintReply(in);
    }

    private static void awaitAndPrintReply(ObjectInputStream in) {
        try {
            Reply reply = (Reply) in.readObject();
            System.out.println(reply.renderMsg());
        } catch (Exception e) {
            System.out.println("Error while retrieving response from server.");
        }
    }
}
