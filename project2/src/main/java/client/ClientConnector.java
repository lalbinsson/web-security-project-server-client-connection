package client;

import common.SSLContextBuilder;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.IOException;
import java.net.UnknownHostException;

public class ClientConnector {
    String host;
    int port;


    public ClientConnector(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public SSLSocket createSocket(String username, String password) throws IOException {
        SSLContext context = SSLContextBuilder.fromUser(username, password);
        SSLSocketFactory factory = context.getSocketFactory();
        return (SSLSocket)factory.createSocket(host, port);
    }




}
