#!/bin/bash

# The tool is very simple
# write sh ./createClient username password
# where username is a unique username
# and password is, well, your password.
# If you write
# sh ./createClient username
# you will be given the default password 
# which is "password"

username=$1
password=$2

if [ "$username" = "" ];
then
    read -p "Username:" username
fi

clientTrustStore="${username}TrustStore.jks"
clientKeyStore="${username}KeyStore.jks"
clientCSR="${username}.csr"
clientCRT="${username}.certificate.pem"

caKey=ca.private.key.pem
caCRT=ca.certificate.pem

#set default password
if [ "$password" = "" ]
then
    password="password"
fi

#if ! [ -f config ]
#then
#    echo "You must specify a config file"
#    exit 1
#fi

#Check if Username is unique
if  ls | grep -Rq $username 
then
    echo "The username \"${username}\" is already taken"
    exit 1
fi

#Check if there is a no ca certificate
if ! [ -f $caCRT ]
then
    echo "Generating a new CA certificate and key"
    openssl req -newkey rsa:2048 -nodes -keyout $caKey -x509 -days 365 -out $caCRT
fi


#-outform PEM

#Generate the client TrustStore and import the certificate
echo "Generate client truststore"
keytool -import -file $caCRT  -alias ca -keystore $clientTrustStore -storepass $password -noprompt

# generate a a key pair and add it to the clientkeystore
keytool -genkeypair -alias $username -keyalg RSA -keystore $clientKeyStore -keysize 2048 -storepass $password -keypass $password -dname "CN=$username OU=Data O=LTH L=Lund ST=Skåne C=SE"

#generate a certificate signing request with the client key
keytool -certreq -alias $username -keystore $clientKeyStore -file $clientCSR -storepass $password

#sign the certificate
openssl x509 -in $clientCSR -out $clientCRT -req -CA $caCRT -CAkey $caKey -days 365 -CAcreateserial

#import the certificates
keytool -importcert -trustcacerts -alias ca -file $caCRT -keystore $clientKeyStore -storepass $password -noprompt
keytool -importcert -trustcacerts -alias $username -file $clientCRT -keystore $clientKeyStore -storepass $password -noprompt

#create dirs if they don't exist
mkdir -p keystores/
mkdir -p certificates/

mv *Store* keystores/
mv ${username}* certificates/

#touch only creates file if it exist
touch -a passwords.txt
echo "${username}:${password}" >> passwords.txt

#important that the ca.srl is kept up to date or we are gonna have problems
git add ca.srl

