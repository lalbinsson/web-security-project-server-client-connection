# Creating users

 The tool is very simple
 ```
 sh ./createClient username password
 ```
 Where username is a unique username and password is, well, your password.
 
 You can omit the password and use the default password which is "password"
 ```
 sh ./createClient username
 ```

 To generate a server credentials you use the same tool, in exactly the same way
 ```
 sh ./createClient servername serverpassword
 ```
